import { Component } from "react";

import background from "../../../assets/images/background.jpg";

class TitleImage extends Component {
    render() {
        return(
            <div className="row mt-3 justify-content-center">
                <div className="col-6 text-center">
                    <img src={background} width={500} alt="Background"/>
                </div>
            </div>
        )
    }
}

export default TitleImage;