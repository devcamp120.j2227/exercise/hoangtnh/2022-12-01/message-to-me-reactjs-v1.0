import { Component } from "react";

class TitleText extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12 text-center">
                    <h1>Chào mừng đến với Devcamp 120</h1>
                </div>
            </div>
        )
    }
}

export default TitleText;