import React, { Component } from "react";
import InputMessage from "./body-input/InputMessage";
import LikeImage from "./body-output/LikeImage";

class Body extends Component {
    render() {
        return(
            <React.Fragment>
                <InputMessage />
                <LikeImage />
            </React.Fragment>
        )
    }
}

export default Body;