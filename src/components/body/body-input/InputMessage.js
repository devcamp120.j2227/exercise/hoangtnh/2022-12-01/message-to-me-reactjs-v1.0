import React, { Component } from "react";

class InputMessage extends Component {

    onBtnClickHandler() {
        console.log("Nút đã được bấm");
    }

    onInputChangeHandler(event) {
        console.log("Input đã được nhập");

        console.log("Giá trị:", event.target.value);
    }

    render() {
        return(
            <React.Fragment>
                 <div className="row mt-3 form-group">
                    <label>Message cho bạn 12 tháng tới:</label>
                    <input placeholder="Nhập message của bạn" className="form-control" onChange={this.onInputChangeHandler}/>
                </div>
                <div className="row mt-3 justify-content-center">
                    <button className="btn btn-success" style={{width: "200px"}} onClick={this.onBtnClickHandler}>Gửi thông điệp</button>
                </div>
            </React.Fragment>
        )
    }
}

export default InputMessage;