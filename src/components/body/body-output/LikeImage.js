import { Component } from "react";

import likeImage from "../../../assets/images/like.png";

class LikeImage extends Component {
    render() {
        return(
            <div className="mt-3 text-center">
                <p>Thông điệp ở đây</p>
                <img src={likeImage} alt="Like" width={100}/>
            </div>
        )
    }
}

export default LikeImage;